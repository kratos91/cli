VENV=venv

venv:
	. ${VENV}/bin/activate
	pip install --upgrade pip