import pytest
from click.testing import CliRunner

from calculator import cli


@pytest.mark.parametrize("test_input,expected",
                         [
                             (["5", "2", "--op", "add"], '5 + 2 = 7'),
                             (["5", "2", "--op", "subtract"], '5 - 2 = 3'),
                             (["5", "2", "--op", "multiply"], '5 * 2 = 10'),
                             (["2", "2", "--op", "divide"], '2 / 2 = 1'),
                         ]
                         )
def test_calculator(test_input, expected):
    runner = CliRunner()
    result = runner.invoke(cli, test_input)

    assert result.exit_code == 0
    assert expected in result.output
