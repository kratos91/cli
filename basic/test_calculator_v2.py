import pytest
from click.testing import CliRunner

from calculator_v2 import cli


@pytest.mark.parametrize("test_input,expected,exit_code",
                         [
                             (["add", "1", "2", " 3"], '1 + 2 + 3 = 6', 0),
                             (["subtract", "5", "2"], '5 - 2 = 3', 0),
                             (["multiply", "5", "2", "1"], '5 * 2 * 1 = 10', 0),
                             (["divide", "2", "2"], '2 / 2 = 1.0', 0),
                             (["add-and-divide", "2", "1", "3"], '(2 + 1) / 3 = 1.0', 0),
                             (["divide", "2", "0"], "Can't divide by Zero!", 2),
                         ]
                         )
def test_calculator(test_input, expected, exit_code):
    runner = CliRunner()
    result = runner.invoke(cli, test_input)

    assert result.exit_code == exit_code
    assert expected in result.output
