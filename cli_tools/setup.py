from setuptools import setup

setup(
    name="cli_tools",
    version='1.0.3',
    py_modules=['greeter', 'calculator', 'authenticate', 'file_utils', 'notes'],
    install_requires=[
        'Click',
        'requests'
    ],
    entry_points={
        'console_scripts': [
            'greetings=greeter:greet',
            'add=calculator:add',
            'subtract=calculator:subtract',
            'authenticate=authenticate:auth',
            'file=file_utils:note',
            'concat=file_utils:concat',
            'notes=notes:main',
            'download=file_utils:download'
        ]
    }
)
