import pytest

from click.testing import CliRunner

from cli_tools.authenticate import auth


def test_non_admin_auth():
    runner = CliRunner()
    prompt_inputs = '\n'.join([
        'emiliano',
        'test',
        'test',
        'N'
    ])

    result = runner.invoke(auth, input=prompt_inputs)

    assert result.exit_code == 0
    expected_output = '\n'.join([
        'Username: emiliano',
        'Password: ',
        'Repeat for confirmation: ',
        'Are you admin? [y/N]: N',
        'Logging in emiliano',
        ''
    ])

    assert result.output == expected_output
