import click


@click.command()
def auth():
    """Provides user authentication."""
    username = click.prompt('Username')
    password = click.prompt('Password', hide_input=True, confirmation_prompt=True)

    if click.confirm('Are you admin?'):
        admin_id = click.prompt('Admin ID', type=int, prompt_suffix='>')
        click.echo(f"Logging in admin {username} (ID={admin_id})")
        return
    click.echo(f"Logging in {username}")
