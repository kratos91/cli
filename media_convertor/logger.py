import click

from proglog import ProgressBarLogger


class MyBarLogger(ProgressBarLogger):
    temp = 0

    def callback(self, **changes):
        # Every time the logger message is updated, this function is called with
        # the `changes` dictionary of the form `parameter: new value`.
        for (parameter, value) in changes.items():
            print('Parameter %s is now %s' % (parameter, value))

    def bars_callback(self, bar, attr, value, old_value=None):
        # Every time the logger progress is updated, this function is called
        fill_char = click.style("#", fg="green")
        empty_char = click.style("-", fg="white", dim=True)
        label_text = "Processing audio..."

        percentage = (value / self.bars[bar]['total']) * 100
        with click.progressbar(label=label_text,
                               length=100,
                               fill_char=fill_char,
                               empty_char=empty_char,
                               show_eta=False) as progress_bar:
            if self.temp != int(percentage):
                progress_bar.update(percentage)
            else:
                self.temp = int(percentage)
