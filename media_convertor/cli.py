import click

from convertor import Convertor
from logger import MyBarLogger


@click.group(invoke_without_command=True)  # Allow users to call our app without
# a command
@click.pass_context
@click.option('--verbose', '-v', is_flag=True, help="Increase output verbosity level")
def main(ctx, verbose):
    group_commands = ['convert', 'play']
    """
            audioConvertor is a command line tool that helps convert video files
            to audio file formats.\n
             example: python cli.py convert -i input/file/path -o output/path
    """

    if ctx.invoked_subcommand is None:
        # No command supplied
        # Inform user on the available commands when running the app

        click.echo("Specify one of the commands below")
        print(*group_commands, sep='\n')

    ctx.obj['VERBOSE'] = verbose


@main.command('convert')
@click.pass_context
@click.option('--input_directory', '-i',
              type=click.Path(exists=True),
              required=True, help="Directory to get files to convert")
@click.option('--output', '-o', nargs=1, type=click.Path(exists=True),
              help="Path to save converted file.\n" +
                   "Defaults to the current working directory if not specified",
              default='.')
def load_files(ctx, input_directory, output):
    """
        :   Convert video file input to audio.
    """
    convertor = Convertor(input_dir=input_directory, output_dir=output)
    if ctx.obj.get('VERBOSE'):
        logger = MyBarLogger()
        convertor.set_logger(logger)
        convertor.convert()
    else:
        convertor.convert()


@main.command('play')
@click.pass_context
@click.option('--playlist', '-p', required=True, type=click.Path(exists=True),
              help="Folder containing audio files to be played")
def load_audio(ctx, playlist):
    """
        :   Selects a track of audio files and loads them up
        in a music player.
    """
    if ctx.obj.get('VERBOSE'):
        click.echo("Verbose")
    else:
        click.echo("Without verbose")


if __name__ == '__main__':
    main(obj={})
