from moviepy.editor import *

from logger import MyBarLogger


class Convertor:
    def __init__(self, logger: MyBarLogger = None, input_dir: str = "", output_dir: str = ""):
        self.logger = logger
        self.input_dir = input_dir
        self.output_dir = output_dir

    def set_logger(self, logger: MyBarLogger = None):
        self.logger = logger

    def convert(self):
        videoclip = VideoFileClip(self.input_dir)
        audioclip = videoclip.audio
        if self.logger is not None:
            audioclip.write_audiofile(f"{self.output_dir}test.mp3", logger=self.logger)
        else:
            audioclip.write_audiofile(f"{self.output_dir}test.mp3")
        audioclip.close()
        videoclip.close()
