## Instalar cli

```shell
python setup.py sdist
pip install .
```

## Correr jarvis
```shell
jarvis os
jarvis ram
```

### Instalar las lineas de comandos
Dentro de la carpeta donde se encuentra setup.py
correr el comando:

```shell
pip install -e .
```